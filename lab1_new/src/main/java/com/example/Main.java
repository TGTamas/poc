package com.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Supported operations: +, -, *, /, min, max, sqrt");
        System.out.print("Enter an operation: ");

        String operation = scanner.nextLine();
        double result;
        double num1, num2;

        switch (operation) {
            case "+":
                System.out.print("Enter the first number: ");
                num1 = scanner.nextDouble();
                System.out.print("Enter the second number: ");
                num2 = scanner.nextDouble();
                result = calculator.add(num1, num2);
                break;
            case "-":
                System.out.print("Enter the first number: ");
                num1 = scanner.nextDouble();
                System.out.print("Enter the second number: ");
                num2 = scanner.nextDouble();
                result = calculator.subtract(num1, num2);
                break;
            case "*":
                System.out.print("Enter the first number: ");
                num1 = scanner.nextDouble();
                System.out.print("Enter the second number: ");
                num2 = scanner.nextDouble();
                result = calculator.multiply(num1, num2);
                break;
            case "/":
                System.out.print("Enter the first number: ");
                num1 = scanner.nextDouble();
                System.out.print("Enter the second number: ");
                num2 = scanner.nextDouble();
                result = calculator.divide(num1, num2);
                break;
            case "min":
                System.out.print("Enter the first number: ");
                num1 = scanner.nextDouble();
                System.out.print("Enter the second number: ");
                num2 = scanner.nextDouble();
                result = calculator.min(num1, num2);
                break;
            case "max":
                System.out.print("Enter the first number: ");
                num1 = scanner.nextDouble();
                System.out.print("Enter the second number: ");
                num2 = scanner.nextDouble();
                result = calculator.max(num1, num2);
                break;
            case "sqrt":
                System.out.print("Enter a number: ");
                num1 = scanner.nextDouble();
                result = calculator.sqrt(num1);
                break;
            default:
                System.out.println("Invalid operation.");
                return;
        }

        System.out.println("Result: " + result);
    }
}

package com.example;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class ChatClient {
    private Socket socket;
    private BufferedReader reader;
    private PrintWriter writer;

    public ChatClient(String serverAddress, int port) throws IOException {
        socket = new Socket(serverAddress, port);
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
    }

    public void start() {
        try {
            // Create a separate thread to handle incoming messages
            new Thread(this::handleIncomingMessages).start();

            // Handle user input for sending messages
            Scanner scanner = new Scanner(System.in);
            System.out.println("Welcome to the Chat Client!");
            System.out.println("Type your messages in the following format: {\"type\":\"text\",\"sender\":\"your_name\",\"content\":\"your_message\"}");

            while (true) {
                System.out.print("Enter your message: ");
                String message = scanner.nextLine();
                sendMessage(message);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void handleIncomingMessages() {
        try {
            while (true) {
                String receivedMessage = reader.readLine();
                System.out.println("Received: " + receivedMessage);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendMessage(String message) {
        writer.println(message);
    }

    public static void main(String[] args) {
        try {
            ChatClient chatClient = new ChatClient("localhost", 12345);
            chatClient.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package com.example;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BigDecimalOperationsTest {

    @Test
    void computeSum() {
        List<BigDecimal> numbers = Arrays.asList(
                new BigDecimal("1.5"),
                new BigDecimal("2.0"),
                new BigDecimal("3.2")
        );

        BigDecimal sum = BigDecimalOperations.computeSum(numbers);

        assertEquals(new BigDecimal("6.7"), sum);
    }

    @Test
    void computeAverage() {
        List<BigDecimal> numbers = Arrays.asList(
                new BigDecimal("1.5"),
                new BigDecimal("2.0"),
                new BigDecimal("3.2")
        );

        BigDecimal average = BigDecimalOperations.computeAverage(numbers);

        assertEquals(new BigDecimal("2.23"), average);
    }

    @Test
    void printTop10Percent() {
        List<BigDecimal> numbers = Arrays.asList(
                new BigDecimal("1.5"),
                new BigDecimal("2.0"),
                new BigDecimal("3.2"),
                new BigDecimal("4.8"),
                new BigDecimal("5.5"),
                new BigDecimal("6.2"),
                new BigDecimal("7.9"),
                new BigDecimal("8.4"),
                new BigDecimal("9.1"),
                new BigDecimal("10.0"),
                new BigDecimal("11.2"),
                new BigDecimal("12.6"),
                new BigDecimal("13.0"),
                new BigDecimal("14.3"),
                new BigDecimal("15.7"),
                new BigDecimal("16.8"),
                new BigDecimal("17.2"),
                new BigDecimal("18.6"),
                new BigDecimal("19.4"),
                new BigDecimal("20.5")
        );

        List<BigDecimal> expectedTop10Percent = Arrays.asList(
                new BigDecimal("19.4"),
                new BigDecimal("20.5")
        );

        final String[] printedOutput = {""};
        System.setOut(new java.io.PrintStream(System.out) {
            public void println(String msg) {
                printedOutput[0] += msg + "\n";
            }
        });

        BigDecimalOperations.printTop10Percent(numbers);

        System.setOut(System.out);

        String printedTop10Percent = printedOutput[0].trim().split("\n")[
                printedOutput[0].trim().split("\n").length - 1];

        assertEquals("Top 10% biggest numbers: " + expectedTop10Percent, printedTop10Percent);
    }
}
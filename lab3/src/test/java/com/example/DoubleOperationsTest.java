package com.example;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

class DoubleOperationsTest {

    @Test
    void computeSumDouble() {
        List<Double> numbers = Arrays.asList(1.5, 2.0, 3.2);

        Double sum = DoubleOperations.computeSumDouble(numbers);

        assertEquals(6.7, sum, 0.0001);
    }

    @Test
    void computeAverageDouble() {
        List<Double> numbers = Arrays.asList(1.5, 2.0, 3.2);
        Double average = DoubleOperations.computeAverageDouble(numbers);
        assertEquals(2.2333, average, 0.0001);
    }

    @Test
    void printTop10PercentDouble() {
        List<Double> numbers = Arrays.asList(1.5, 2.0, 3.2, 4.8, 5.5, 6.2, 7.9, 8.4, 9.1, 10.0, 11.2, 12.6);
        List<Double> expectedTop10Percent = Arrays.asList(12.6);

        final String[] printedOutput = {""};
        System.setOut(new java.io.PrintStream(System.out) {
            public void println(String msg) {
                printedOutput[0] += msg + "\n";
            }
        });

        DoubleOperations.printTop10PercentDouble(numbers);

        System.setOut(System.out);
        String printedTop10Percent = printedOutput[0].trim().split("\n")[
                printedOutput[0].trim().split("\n").length - 1];

        assertEquals("Top 10% biggest numbers (Double): " + expectedTop10Percent, printedTop10Percent);
    }

    @Test
    void computeSumPrimitive() {
        double[] numbers = {1.5, 2.0, 3.2};

        double sum = DoubleOperations.computeSumPrimitive(numbers);

        assertEquals(6.7, sum, 0.0001);
    }

    @Test
    void computeAveragePrimitive() {
        double[] numbers = {1.5, 2.0, 3.2};

        double average = DoubleOperations.computeAveragePrimitive(numbers);

        assertEquals(2.2333, average, 0.0001);
    }

    @Test
    void printTop10PercentPrimitive() {
        double[] numbers = {1.5, 2.0, 3.2, 4.8, 5.5, 6.2, 7.9, 8.4, 9.1, 10.0, 11.2, 12.6};
        double[] expectedTop10Percent = {12.6};

        final String[] printedOutput = {""};
        System.setOut(new java.io.PrintStream(System.out) {
            public void println(String msg) {
                printedOutput[0] += msg + "\n";
            }
        });

        DoubleOperations.printTop10PercentPrimitive(numbers);

        System.setOut(System.out);

        String expectedOutput = "Top 10% biggest numbers (double): " + Arrays.toString(expectedTop10Percent);
        assertEquals(expectedOutput, printedOutput[0].trim());
    }
}

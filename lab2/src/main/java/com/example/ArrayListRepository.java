package com.example;
import java.util.ArrayList;
import java.util.List;

public class ArrayListRepository<T> implements InMemoryRepository<T> {
    private List<T> list = new ArrayList<>();

    @Override
    public boolean add(T item) {
        return list.add(item);
    }

    @Override
    public boolean remove(T item) {
        return list.remove(item);
    }

    @Override
    public boolean contains(T item) {
        return list.contains(item);
    }
}


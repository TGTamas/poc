package com.example;

import org.json.JSONObject;

import java.io.IOException;
import java.net.*;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Peer {
    private String name;
    private int port;

    private final ExecutorService executorService = Executors.newFixedThreadPool(3);

    public Peer(String name, int port) throws BindException {
        this.name = name;
        this.port = port;
    }

    private static boolean unavailablePort(int port) {
        try (DatagramSocket ignored = new DatagramSocket(port)) {
            return false;
        } catch (BindException e) {
            return true;
        } catch (SocketException e) {
            e.printStackTrace();
            return true;
        }
    }

    public void start() {
        executorService.execute(this::receiveMessages);
        executorService.execute(this::sendMessages);
    }

    private void receiveMessages() {
        try {
            DatagramSocket socket = new DatagramSocket(port);
            byte[] buffer = new byte[1024];

            while (true) {
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                socket.receive(packet);

                JSONObject jsonMessage = new JSONObject(new String(packet.getData(), 0, packet.getLength()));

                String senderName = jsonMessage.getString("username");
                String message = jsonMessage.getString("message");

                System.out.println("\nReceived message from " + senderName + ": " + message + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendMessages() {
        try {
            Scanner scanner = new Scanner(System.in);

            while (true) {
                System.out.print("Enter message: ");
                String message = scanner.nextLine();

                if ("exit".equalsIgnoreCase(message.trim())) {
                    shutdown();
                    System.exit(0);
                }

                System.out.print("Enter recipient's ports (comma-separated): ");
                String portsInput = scanner.nextLine();

                try {
                    List<String> portStrings = Arrays.asList(portsInput.split(","));
                    List<Integer> recipientPorts = parsePortList(portStrings);

                    JSONObject jsonMessage = new JSONObject();
                    jsonMessage.put("username", name);
                    jsonMessage.put("message", message);

                    sendMessage(jsonMessage, recipientPorts);
                } catch (NumberFormatException e) {
                    System.err.println("Error: Invalid port format. Please enter valid integers separated by commas.");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<Integer> parsePortList(List<String> portStrings) {
        return Arrays.stream(portStrings.toArray(new String[0]))
                .map(Integer::parseInt)
                .toList();
    }

    private void sendMessage(JSONObject jsonMessage, List<Integer> recipientPorts) {
        try {
            DatagramSocket socket = new DatagramSocket();

            InetAddress recipientAddress = InetAddress.getByName("localhost");

            byte[] data = jsonMessage.toString().getBytes();

            for (int recipientPort : recipientPorts) {
                DatagramPacket packet = new DatagramPacket(data, data.length, recipientAddress, recipientPort);
                socket.send(packet);
            }

            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void shutdown() {
        executorService.shutdown();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Peer peer = null;

        while (peer == null) {
            try {
                System.out.print("Enter your name: ");
                String name = scanner.nextLine();

                int port;

                do {
                    System.out.print("Enter the port: ");
                    port = Integer.parseInt(scanner.nextLine());

                    if (unavailablePort(port)) {
                        System.err.println("Error: Port is already in use. Please choose another port.");
                    }
                } while (unavailablePort(port));

                peer = new Peer(name, port);
                peer.start();
            } catch (NumberFormatException e) {
                System.err.println("Error: Invalid port format. Please enter a valid integer.");
            } catch (BindException e) {
                throw new RuntimeException(e);
            }
        }

        Runtime.getRuntime().addShutdownHook(new Thread(peer::shutdown));
    }
}

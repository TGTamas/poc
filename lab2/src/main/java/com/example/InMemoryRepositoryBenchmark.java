package com.example;
import org.openjdk.jmh.annotations.*;

@State(Scope.Benchmark)
public class InMemoryRepositoryBenchmark {
    private InMemoryRepository<String> hashSetRepository = new HashSetRepository<>();
    private InMemoryRepository<String> arrayListRepository = new ArrayListRepository<>();
    private InMemoryRepository<String> treeSetRepository = new TreeSetRepository<>();
    private InMemoryRepository<String> concurrentHashmapRepository = new ConcurrentHashMapRepository<>();
    private InMemoryRepository<String> eclipseCollectionsRepository = new EclipseCollectionsRepository<>();
    private InMemoryRepository<String> fastutilRepository = new FastutilRepository<>();
    private InMemoryRepository<String> kolobokeRepository = new KolobokeRepository<>();

    @Benchmark
    public void benchmarkAddHashSet() {
        hashSetRepository.add("item");
    }

    @Benchmark
    public void benchmarkRemoveHashSet() {
        hashSetRepository.remove("item");
    }

    @Benchmark
    public void benchmarkContainsHashSet() {
        hashSetRepository.contains("item");
    }
}


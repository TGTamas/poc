package com.example;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;

public class FastutilRepository<T> implements InMemoryRepository<T> {
    private ObjectOpenHashSet<T> set = new ObjectOpenHashSet<>();

    @Override
    public boolean add(T item) {
        return set.add(item);
    }

    @Override
    public boolean remove(T item) {
        return set.remove(item);
    }

    @Override
    public boolean contains(T item) {
        return set.contains(item);
    }
}


package com.example;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    @Test
    void testAdd() {
        Calculator calculator = new Calculator();
        double result = calculator.add(2.0, 3.0);
        assertEquals(5.0, result, 0.001);
    }

    @Test
    void testSubtract() {
        Calculator calculator = new Calculator();
        double result = calculator.subtract(5.0, 3.0);
        assertEquals(2.0, result, 0.001);
    }

    @Test
    void testMultiply() {
        Calculator calculator = new Calculator();
        double result = calculator.multiply(4.0, 3.0);
        assertEquals(12.0, result, 0.001);
    }

    @Test
    void testDivide() {
        Calculator calculator = new Calculator();
        double result = calculator.divide(10.0, 2.0);
        assertEquals(5.0, result, 0.001);
    }

    @Test
    void testMin() {
        Calculator calculator = new Calculator();
        double result = calculator.min(5.0, 3.0);
        assertEquals(3.0, result, 0.001);
    }

    @Test
    void testMax() {
        Calculator calculator = new Calculator();
        double result = calculator.max(5.0, 3.0);
        assertEquals(5.0, result, 0.001);
    }

    @Test
    void testSqrt() {
        Calculator calculator = new Calculator();
        double result = calculator.sqrt(25.0);
        assertEquals(5.0, result, 0.001);
    }
}
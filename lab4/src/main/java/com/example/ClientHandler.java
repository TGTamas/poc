package com.example;

import org.json.JSONObject;

import java.io.*;
import java.net.Socket;

public class ClientHandler extends Thread {
    private Socket clientSocket;
    private BufferedReader reader;
    private PrintWriter writer;

    public ClientHandler(Socket clientSocket) throws IOException {
        this.clientSocket = clientSocket;
        this.reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        this.writer = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream()), true);
    }

    @Override
    public void run() {
        try {
            while (true) {
                String clientMessage = reader.readLine();
                if (clientMessage == null) {
                    // Connection closed by the client
                    break;
                }

                // Assuming messages are JSON strings
                JSONObject jsonMessage = new JSONObject(clientMessage);

                // Process the JSON message
                String messageType = jsonMessage.getString("type");
                if (messageType.equals("text")) {
                    String sender = jsonMessage.getString("sender");
                    String content = jsonMessage.getString("content");
                    System.out.println("Received message from " + sender + ": " + content);

                    // Example: Send a response back to the client
                    JSONObject response = new JSONObject();
                    response.put("type", "ack");
                    response.put("content", "Message received successfully");
                    writer.println(response.toString());
                } else {
                    // Handle other message types as needed
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

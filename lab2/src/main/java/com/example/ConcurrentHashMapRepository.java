package com.example;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapRepository<T> implements InMemoryRepository<T> {
    private ConcurrentHashMap<T, Boolean> map = new ConcurrentHashMap<>();

    @Override
    public boolean add(T item) {
        return map.put(item, Boolean.TRUE) == null;
    }

    @Override
    public boolean remove(T item) {
        return map.remove(item) != null;
    }

    @Override
    public boolean contains(T item) {
        return map.containsKey(item);
    }
}


package com.example;
import java.util.HashSet;
import java.util.Set;

public class HashSetRepository<T> implements InMemoryRepository<T> {
    private Set<T> set = new HashSet<>();

    @Override
    public boolean add(T item) {
        return set.add(item);
    }

    @Override
    public boolean remove(T item) {
        return set.remove(item);
    }

    @Override
    public boolean contains(T item) {
        return set.contains(item);
    }
}


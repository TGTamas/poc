package com.example;

public interface InMemoryRepository<T> {
    boolean add(T item);
    boolean remove(T item);
    boolean contains(T item);
}


package com.example;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class DoubleOperations {
    public static Double computeSumDouble(List<Double> numbers) {
        return numbers.stream().mapToDouble(Double::doubleValue).sum();
    }

    public static Double computeAverageDouble(List<Double> numbers) {
        return numbers.stream().mapToDouble(Double::doubleValue).average().orElse(0.0);
    }

    public static void printTop10PercentDouble(List<Double> numbers) {
        int size = (int) (numbers.size() * 0.1);
        List<Double> top10Percent = numbers.stream()
                .sorted(Double::compare)
                .skip(numbers.size() - size)
                .collect(Collectors.toList());
        System.out.println("Top 10% biggest numbers (Double): " + top10Percent);
    }

    public static double computeSumPrimitive(double[] numbers) {
        return Arrays.stream(numbers).sum();
    }

    public static double computeAveragePrimitive(double[] numbers) {
        return Arrays.stream(numbers).average().orElse(0.0);
    }

    public static void printTop10PercentPrimitive(double[] numbers) {
        int size = (int) (numbers.length * 0.1);
        double[] top10Percent = Arrays.stream(numbers)
                .sorted()
                .skip(numbers.length - size)
                .toArray();
        System.out.println("Top 10% biggest numbers (double): " + Arrays.toString(top10Percent));
    }

    public static List<Double> generateRandomDoubles(int size) {
        Random random = new Random();
        return random.doubles(size, 0, 1000).boxed().collect(Collectors.toList());
    }
    public static double[] generateRandomDoublePrimitives(int size) {
        Random random = new Random();
        return random.doubles(size, 0, 1000).toArray();
    }


    public static void main(String[] args) {

        List<Double> randomDoubles = generateRandomDoubles(50);
        System.out.println("Double Sum: " + computeSumDouble(randomDoubles));
        System.out.println("Double Average: " + computeAverageDouble(randomDoubles));
        printTop10PercentDouble(randomDoubles);

        double[] randomDoublePrimitives = generateRandomDoublePrimitives(50);
        System.out.println("Primitive double Sum: " + computeSumPrimitive(randomDoublePrimitives));
        System.out.println("Primitive double Average: " + computeAveragePrimitive(randomDoublePrimitives));
        printTop10PercentPrimitive(randomDoublePrimitives);
    }
}

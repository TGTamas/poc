package com.example;
import java.util.TreeSet;

public class TreeSetRepository<T> implements InMemoryRepository<T> {
    private TreeSet<T> set = new TreeSet<>();

    @Override
    public boolean add(T item) {
        return set.add(item);
    }

    @Override
    public boolean remove(T item) {
        return set.remove(item);
    }

    @Override
    public boolean contains(T item) {
        return set.contains(item);
    }
}

package com.example;

import java.math.BigDecimal;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class BigDecimalOperations {

    public static BigDecimal computeSum(List<BigDecimal> numbers) {
        return numbers.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public static BigDecimal computeAverage(List<BigDecimal> numbers) {
        return numbers.stream().reduce(BigDecimal.ZERO, BigDecimal::add)
                .divide(BigDecimal.valueOf(numbers.size()), 2, BigDecimal.ROUND_HALF_UP);
    }

    public static void printTop10Percent(List<BigDecimal> numbers) {
        int size = (int) (numbers.size() * 0.1);
        List<BigDecimal> top10Percent = numbers.stream()
                .sorted(BigDecimal::compareTo)
                .skip(numbers.size() - size)
                .collect(Collectors.toList());
        System.out.println("Top 10% biggest numbers: " + top10Percent);
    }

    public static void main(String[] args) {
        List<BigDecimal> numbers = generateRandomNumbers(50);

        System.out.println("Sum: " + computeSum(numbers));

        System.out.println("Average: " + computeAverage(numbers));

        printTop10Percent(numbers);
    }

    public static List<BigDecimal> generateRandomNumbers(int size) {
        Random random = new Random(42);
        return random.doubles(size, 0, 1000)
                .mapToObj(BigDecimal::valueOf)
                .collect(Collectors.toList());
    }
}
